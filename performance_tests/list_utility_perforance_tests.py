#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys

sys.path.append("..")

import os
import timeit
from ListUtility import ListUtility

def getList(start, end):
    l = [1,2]
    while start <= end:
        l.append(start)
        start += 2
    return l

small_list1 = [1,2,3]
small_list2 = [1,2,4]
medium_list1 = [1,2,3,5,7,9,11,13,15,17,19]
medium_list2 = [1,2,4,6,8,10,12,14,16,18,20]
large_list1 = [1,2,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39]
large_list2 = [1,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40]
huge_list1 = getList(3, 999)
huge_list2 = getList(4, 1000)

large_string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
sub_strings  = ["z", "A", "0"]

def small_diff():
    return ListUtility.GetDiff(small_list1, small_list2)

def medium_diff():
    return ListUtility.GetDiff(medium_list1, medium_list2)

def large_diff():
    return ListUtility.GetDiff(large_list1, large_list2)

def huge_diff():
    return ListUtility.GetDiff(huge_list1, huge_list2)

def small_difflc():
    return ListUtility.GetDiffLC(small_list1, small_list2)

def medium_difflc():
    return ListUtility.GetDiffLC(medium_list1, medium_list2)

def large_difflc():
    return ListUtility.GetDiffLC(large_list1, large_list2)

def huge_difflc():
    return ListUtility.GetDiffLC(huge_list1, huge_list2)

def small_diffs():
    return ListUtility.GetDiffS(small_list1, small_list2)

def medium_diffs():
    return ListUtility.GetDiffS(medium_list1, medium_list2)

def large_diffs():
    return ListUtility.GetDiffS(large_list1, large_list2)

def huge_diffs():
    return ListUtility.GetDiffS(huge_list1, huge_list2)

def small_verbose_diff():
    return ListUtility.GetVerboseDiff(small_list1, small_list2)

def medium_verbose_diff():
    return ListUtility.GetVerboseDiff(medium_list1, medium_list2)

def large_verbose_diff():
    return ListUtility.GetVerboseDiff(large_list1, large_list2)

def huge_verbose_diff():
    return ListUtility.GetVerboseDiff(huge_list1, huge_list2)

def small_verbose_diffs():
    return ListUtility.GetVerboseDiffS(small_list1, small_list2)

def medium_verbose_diffs():
    return ListUtility.GetVerboseDiffS(medium_list1, medium_list2)

def large_verbose_diffs():
    return ListUtility.GetVerboseDiffS(large_list1, large_list2)

def huge_verbose_diffs():
    return ListUtility.GetVerboseDiffS(huge_list1, huge_list2)

def one_of():
    return ListUtility.HasOneOf(huge_list1, [997, 999])

def one_ofs():
    return ListUtility.HasOneOfS(huge_list1, [997, 999])

def all_of():
    return ListUtility.HasAllOf(huge_list1, large_list1)

def one_of_sub_string():
    return ListUtility.HasAllOf(large_string, sub_strings)

def all_of_sub_string():
    return ListUtility.HasAllOf(large_string, sub_strings)

def all__any_of_sub_string():
    return ListUtility.HasAllAnySubString(large_string, ["a", "b"], ["C", "0"])

if __name__ == '__main__':

    # Small list timings for GetDiff.
    print("GetDiff(small_list1, small_list2) time: {}".format(timeit.timeit(small_diff, number=10000)))
    print("GetDiffLC(small_list1, small_list2) time: {}".format(timeit.timeit(small_difflc, number=10000)))
    print("GetDiffS(small_list1, small_list2) time: {}\n".format(timeit.timeit(small_diffs, number=10000)))

    # Medium list timings for GetDiff.
    print("GetDiff(medium_list1, medium_list2) time: {}".format(timeit.timeit(medium_diff, number=10000)))
    print("GetDiffLC(medium_list1, medium_list2) time: {}".format(timeit.timeit(medium_difflc, number=10000)))
    print("GetDiffS(medium_list1, medium_list2) time: {}\n".format(timeit.timeit(medium_diffs, number=10000)))

    # Large list timings for GetDiff.
    print("GetDiff(large_list1, large_list2) time: {}".format(timeit.timeit(large_diff, number=10000)))
    print("GetDiffLC(large_list1, large_list2) time: {}".format(timeit.timeit(large_difflc, number=10000)))
    print("GetDiffS(large_list1, large_list2) time: {}\n".format(timeit.timeit(large_diffs, number=10000)))

    # Huge list timings for GetDiff.
    print("GetDiff(huge_list1, huge_list2) time: {}".format(timeit.timeit(huge_diff, number=200)))
    print("GetDiffLC(huge_list1, huge_list2) time: {}".format(timeit.timeit(huge_difflc, number=200)))
    print("GetDiffS(huge_list1, huge_list2) time: {}\n".format(timeit.timeit(huge_diffs, number=200)))

    # Small list timings for GetVerboseDiff.
    print("GetVerboseDiff(small_list1, small_list2) time: {}".format(timeit.timeit(small_verbose_diff, number=10000)))
    print("GetVerboseDiffS(small_list1, small_list2) time: {}\n".format(timeit.timeit(small_verbose_diffs, number=10000)))

    # Medium list timings for GetVerboseDiff.
    print("GetVerboseDiff(medium_list1, medium_list2) time: {}".format(timeit.timeit(medium_verbose_diff, number=10000)))
    print("GetVerboseDiffS(medium_list1, medium_list2) time: {}\n".format(timeit.timeit(medium_verbose_diffs, number=10000)))

    # Large list timings for GetVerboseDiff.
    print("GetVerboseDiff(large_list1, large_list2) time: {}".format(timeit.timeit(large_verbose_diff, number=10000)))
    print("GetVerboseDiffS(large_list1, large_list2) time: {}\n".format(timeit.timeit(large_verbose_diffs, number=10000)))

    # Huge list timings for GetVerboseDiff.
    print("GetVerboseDiff(huge_list1, huge_list2) time: {}".format(timeit.timeit(huge_verbose_diff, number=200)))
    print("GetVerboseDiffS(huge_list1, huge_list2) time: {}\n".format(timeit.timeit(huge_verbose_diffs, number=200)))

    # Huge list for HasOneOf.
    print("HasOneOf(huge_list1, [997, 999]) time: {}".format(timeit.timeit(one_of, number=1000)))
    print("HasOneOfS(huge_list1, [997, 999]) time: {}\n".format(timeit.timeit(one_ofs, number=1000)))

    # Huge list for HasAllOf.
    print("HasAllOf(huge_list1, large_list1) time: {}\n".format(timeit.timeit(all_of, number=1000)))

    # HasOneOfSubString.
    print("HasOneOfSubString({}, {}) time: {}\n".format(large_string, sub_strings, timeit.timeit(one_of_sub_string, number=10000)))

    # HasAllOfSubString.
    print("HasAllOfSubString({}, {}) time: {}\n".format(large_string, sub_strings, timeit.timeit(all_of_sub_string, number=10000)))

    # HasAllAnyOfSubString.
    print("HasAllAnyOfSubString({}, {}, {}) time: {}\n".format(large_string, ["a", "b"], ["C", "0"], timeit.timeit(all_of_sub_string, number=10000)))
