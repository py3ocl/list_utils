#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys

sys.path.append("..")

import os
from ListUtility import ListUtility

import unittest

class TestFileFinderMethods(unittest.TestCase):

    def test_GetDiff(self):
        f = ListUtility.GetDiff
        self.assertTrue(f([], []) == [])
        self.assertTrue(f([1], []) == [1])
        self.assertTrue(f([], [1]) == [1])
        self.assertTrue(f([1], [1]) == [])
        self.assertTrue(f([1,2], [1]) == [2])
        self.assertTrue(f([1], [1,2]) == [2])
        self.assertTrue(f([1,2], [1,2]) == [])
        self.assertTrue(f([1,2,3], [1,2,4]) == [3,4])

    def test_GetDiffLC(self):
        f = ListUtility.GetDiffLC
        self.assertTrue(f([], []) == [])
        self.assertTrue(f([1], []) == [1])
        self.assertTrue(f([], [1]) == [1])
        self.assertTrue(f([1], [1]) == [])
        self.assertTrue(f([1,2], [1]) == [2])
        self.assertTrue(f([1], [1,2]) == [2])
        self.assertTrue(f([1,2], [1,2]) == [])
        self.assertTrue(f([1,2,3], [1,2,4]) == [3,4])

    def test_GetDiffS(self):
        f = ListUtility.GetDiffS
        self.assertTrue(f([], []) == [])
        self.assertTrue(f([1], []) == [1])
        self.assertTrue(f([], [1]) == [1])
        self.assertTrue(f([1], [1]) == [])
        self.assertTrue(f([1,2], [1]) == [2])
        self.assertTrue(f([1], [1,2]) == [2])
        self.assertTrue(f([1,2], [1,2]) == [])
        self.assertTrue(f([1,2,3], [1,2,4]) == [3,4])

    def test_GetVerboseDiff(self):
        f = ListUtility.GetVerboseDiff
        self.assertTrue(f([], []) == [])
        self.assertTrue(f([1], []) == [(1,0,1)])
        self.assertTrue(f([], [1]) == [(0,1,1)])
        self.assertTrue(f([1], [1]) == [])
        self.assertTrue(f([1,2], [1]) == [(2,0,2)])
        self.assertTrue(f([1], [1,2]) == [(0,2,2)])
        self.assertTrue(f([1,2], [1,2]) == [])
        self.assertTrue(f([1,2,3], [1,2,4]) == [(3,0,3),(0,3,4)])

    def test_GetVerboseDiffS(self):
        f = ListUtility.GetVerboseDiffS
        self.assertTrue(f([], []) == [])
        self.assertTrue(f([1], []) == [(1,0,1)])
        self.assertTrue(f([], [1]) == [(0,1,1)])
        self.assertTrue(f([1], [1]) == [])
        self.assertTrue(f([1,2], [1]) == [(2,0,2)])
        self.assertTrue(f([1], [1,2]) == [(0,2,2)])
        self.assertTrue(f([1,2], [1,2]) == [])
        self.assertTrue(f([1,2,3], [1,2,4]) == [(3,0,3),(0,3,4)])

    def test_GetLeftDiff(self):
        f = ListUtility.GetLeftDiff
        self.assertTrue(f([], []) == [])
        self.assertTrue(f([1], []) == [1])
        self.assertTrue(f([], [1]) == [])
        self.assertTrue(f([1], [1]) == [])
        self.assertTrue(f([1,2], [1]) == [2])
        self.assertTrue(f([1], [1,2]) == [])
        self.assertTrue(f([1,2], [1,2]) == [])
        self.assertTrue(f([1,2,3], [1,2,4]) == [3])

    def test_GetLeftDiffLC(self):
        f = ListUtility.GetLeftDiffLC
        self.assertTrue(f([], []) == [])
        self.assertTrue(f([1], []) == [1])
        self.assertTrue(f([], [1]) == [])
        self.assertTrue(f([1], [1]) == [])
        self.assertTrue(f([1,2], [1]) == [2])
        self.assertTrue(f([1], [1,2]) == [])
        self.assertTrue(f([1,2], [1,2]) == [])
        self.assertTrue(f([1,2,3], [1,2,4]) == [3])

    def test_GetLeftDiffS(self):
        f = ListUtility.GetLeftDiffS
        self.assertTrue(f([], []) == [])
        self.assertTrue(f([1], []) == [1])
        self.assertTrue(f([], [1]) == [])
        self.assertTrue(f([1], [1]) == [])
        self.assertTrue(f([1,2], [1]) == [2])
        self.assertTrue(f([1], [1,2]) == [])
        self.assertTrue(f([1,2], [1,2]) == [])
        self.assertTrue(f([1,2,3], [1,2,4]) == [3])

    def test_GetRightDiff(self):
        f = ListUtility.GetRightDiff
        self.assertTrue(f([], []) == [])
        self.assertTrue(f([1], []) == [])
        self.assertTrue(f([], [1]) == [1])
        self.assertTrue(f([1], [1]) == [])
        self.assertTrue(f([1,2], [1]) == [])
        self.assertTrue(f([1], [1,2]) == [2])
        self.assertTrue(f([1,2], [1,2]) == [])
        self.assertTrue(f([1,2,3], [1,2,4]) == [4])

    def test_GetRightDiffLC(self):
        f = ListUtility.GetRightDiffLC
        self.assertTrue(f([], []) == [])
        self.assertTrue(f([1], []) == [])
        self.assertTrue(f([], [1]) == [1])
        self.assertTrue(f([1], [1]) == [])
        self.assertTrue(f([1,2], [1]) == [])
        self.assertTrue(f([1], [1,2]) == [2])
        self.assertTrue(f([1,2], [1,2]) == [])
        self.assertTrue(f([1,2,3], [1,2,4]) == [4])

    def test_GetRightDiffS(self):
        f = ListUtility.GetRightDiffS
        self.assertTrue(f([], []) == [])
        self.assertTrue(f([1], []) == [])
        self.assertTrue(f([], [1]) == [1])
        self.assertTrue(f([1], [1]) == [])
        self.assertTrue(f([1,2], [1]) == [])
        self.assertTrue(f([1], [1,2]) == [2])
        self.assertTrue(f([1,2], [1,2]) == [])
        self.assertTrue(f([1,2,3], [1,2,4]) == [4])

    def test_HasOneOf(self):
        f = ListUtility.HasOneOf
        self.assertFalse(f([], []))
        self.assertFalse(f([], [2]))
        self.assertFalse(f([1], [2]))
        self.assertFalse(f([1,3], [2,4]))

        self.assertTrue(f([1], [1,2]))
        self.assertTrue(f([1], [1,2]))
        self.assertTrue(f([2], [1,2]))
        self.assertTrue(f([4,3,2], [1,2]))

    def test_HasOneOfS(self):
        f = ListUtility.HasOneOfS
        self.assertFalse(f([], []))
        self.assertFalse(f([], [2]))
        self.assertFalse(f([1], [2]))
        self.assertFalse(f([1,3], [2,4]))

        self.assertTrue(f([1], [1,2]))
        self.assertTrue(f([1], [1,2]))
        self.assertTrue(f([2], [1,2]))
        self.assertTrue(f([4,3,2], [1,2]))

    def test_HasAllOf(self):
        f = ListUtility.HasAllOf
        self.assertFalse(f([], []))
        self.assertFalse(f([1], []))
        self.assertFalse(f([], [2]))
        self.assertFalse(f([1], [2]))
        self.assertFalse(f([1], [1,2]))
        self.assertFalse(f([1,3], [2,4]))
        self.assertFalse(f([1,3], [1,2,3]))

        self.assertTrue(f([1], [1]))
        self.assertTrue(f([1,2], [1]))
        self.assertTrue(f([1,2], [1,2]))
        self.assertTrue(f([1,2,3], [1,2]))
        self.assertTrue(f([0,1,2,3], [1,2]))

    def test_HasOneOfSubString(self):
        f = ListUtility.HasOneOfSubString
        self.assertFalse(f("", []))
        self.assertFalse(f("", [""]))
        self.assertFalse(f("", ["a"]))
        self.assertFalse(f("a", []))
        self.assertFalse(f("a", [""]))
        self.assertFalse(f("a b", ["ab"]))

        self.assertTrue(f("a", ["a"]))
        self.assertTrue(f("ab", ["a"]))
        self.assertTrue(f("ab", ["ab"]))
        self.assertTrue(f("ab", ["a","b"]))
        self.assertTrue(f("a b", ["a","b"]))

    def test_HasAllOfSubString(self):
        f = ListUtility.HasAllOfSubString
        self.assertFalse(f("", []))
        self.assertFalse(f("", [""]))
        self.assertFalse(f("", ["a"]))
        self.assertFalse(f("a", []))
        self.assertFalse(f("a", [""]))
        self.assertFalse(f("a", ["b"]))
        self.assertFalse(f("a", ["a","b"]))
        self.assertFalse(f("a b", ["ab"]))

        self.assertTrue(f("a", ["a"]))
        self.assertTrue(f("ab", ["ab"]))
        self.assertTrue(f("ab", ["a","b"]))
        self.assertTrue(f("a b", ["a","b"]))
        self.assertTrue(f("a b", ["a b"]))
        self.assertTrue(f("abc", ["a","b"]))
        self.assertTrue(f("abc", ["a","c"]))
        self.assertTrue(f("a b c", ["a","b"]))
        self.assertTrue(f("a b c", ["a","c"]))

    def test_HasAllAnySubString(self):
        f = ListUtility.HasAllAnySubString

        self.assertFalse(f("", [], []))
        self.assertFalse(f("", ["a"], []))
        self.assertFalse(f("", [], ["a"]))
        self.assertFalse(f("", ["a"], ["a"]))
        self.assertFalse(f("a", [], []))
        self.assertFalse(f("a", ["a", "b"], []))
        self.assertFalse(f("ab", ["a", "b"], []))

        self.assertTrue(f("ab", ["a"], ["b"]))
        self.assertTrue(f("a b", ["a"], ["b"]))
        self.assertTrue(f("abc", ["ab"], ["c"]))
        self.assertTrue(f("abc", ["a","b"], ["c"]))
        self.assertTrue(f("abc", ["a"], ["b","c"]))

if __name__ == '__main__':
    unittest.main()
