#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class ListUtility:

    ## Get all differences between both lists.
    ## Examples:
    ##   [], [] == []
    ##   [1], [] == [1]
    ##   [], [1] == [1]
    ##   [1], [1] == []
    @staticmethod
    def GetDiff(list1, list2):
        diff = []
        for item in list1:
            if item not in list2:
                diff.append(item)
        for item in list2:
            if item not in list1:
                diff.append(item)
        return diff

    ## Get all differences between both lists using list comprehension.
    ## Note: This is faster that GetDiff for larger lists.
    ## Examples:
    ##   [], [] == []
    ##   [1], [] == [1]
    ##   [], [1] == [1]
    ##   [1], [1] == []
    @staticmethod
    def GetDiffLC(list1, list2):
        return [i1 for i1 in list1 if i1 not in list2] + [i2 for i2 in list2 if i2 not in list1]

    ## Get all differences between both lists using a set for lookup.
    ## Note: This is faster that GetDiff and GetDiffLS for larger lists.
    ## Examples:
    ##   [], [] == []
    ##   [1], [] == [1]
    ##   [], [1] == [1]
    ##   [1], [1] == []
    @staticmethod
    def GetDiffS(list1, list2):
        s1 = set(list1)
        s2 = set(list2)
        diff = []
        for item in list1:
            if item not in s2:
                diff.append(item)
        for item in list2:
            if item not in s1:
                diff.append(item)
        return diff

    ## Get all differences between both lists.
    ## Return the with each element containing the left or right position,
    ## and the item that's different.
    ## Examples:
    ##   [], [] == []
    ##   [1], [] == [(1,0,1)]
    ##   [], [1] == [(0,1,1)]
    ##   [1], [1] == []
    @staticmethod
    def GetVerboseDiff(list1, list2):
        diff = []
        list1_pos = 1
        list2_pos = 1
        for item in list1:
            if item not in list2:
                diff.append((list1_pos, 0, item))
            list1_pos += 1
        for item in list2:
            if item not in list1:
                diff.append((0, list2_pos, item))
            list2_pos += 1
        return diff

    ## Get all differences between both lists using a set for lookup.
    ## Note: This is faster that GetDiff and GetDiffLS for larger lists.
    ## Examples:
    ##   [], [] == []
    ##   [1], [] == [(1,0,1)]
    ##   [], [1] == [(0,1,1)]
    ##   [1], [1] == []
    @staticmethod
    def GetVerboseDiffS(list1, list2):
        s1 = set(list1)
        s2 = set(list2)
        diff = []
        list1_pos = 1
        list2_pos = 1
        for item in list1:
            if item not in s2:
                diff.append((list1_pos, 0, item))
            list1_pos += 1
        for item in list2:
            if item not in s1:
                diff.append((0, list2_pos, item))
            list2_pos += 1
        return diff

    ## Get only differences where list1 contains items not in list2.
    ## Examples:
    ##   [], [] == []
    ##   [1], [] == [1]
    ##   [], [1] == []
    ##   [1], [1] == []
    @staticmethod
    def GetLeftDiff(list1, list2):
        diff = []
        for item in list1:
            if item not in list2:
                diff.append(item)
        return diff

    ## Get only differences where list1 contains items not in list2.
    ## Examples:
    ##   [], [] == []
    ##   [1], [] == [1]
    ##   [], [1] == []
    ##   [1], [1] == []
    @staticmethod
    def GetLeftDiffLC(list1, list2):
        return [i1 for i1 in list1 if i1 not in list2]

    ## Get only differences where list1 contains items not in list2.
    ## Examples:
    ##   [], [] == []
    ##   [1], [] == [1]
    ##   [], [1] == []
    ##   [1], [1] == []
    @staticmethod
    def GetLeftDiffS(list1, list2):
        s2 = set(list2)
        diff = []
        for item in list1:
            if item not in s2:
                diff.append(item)
        return diff

    ## Get only differences where list1 contains items not in list2.
    ## Examples:
    ##   [], [] == []
    ##   [1], [] == []
    ##   [], [1] == [1]
    ##   [1], [1] == []
    @staticmethod
    def GetRightDiff(list1, list2):
        diff = []
        for item in list2:
            if item not in list1:
                diff.append(item)
        return diff

    ## Get only differences where list1 contains items not in list2.
    ## Examples:
    ##   [], [] == []
    ##   [1], [] == []
    ##   [], [1] == [1]
    ##   [1], [1] == []
    @staticmethod
    def GetRightDiffLC(list1, list2):
        return [i2 for i2 in list2 if i2 not in list1]

    ## Get only differences where list1 contains items not in list2.
    ## Examples:
    ##   [], [] == []
    ##   [1], [] == []
    ##   [], [1] == [1]
    ##   [1], [1] == []
    @staticmethod
    def GetRightDiffS(list1, list2):
        s1 = set(list1)
        diff = []
        for item in list2:
            if item not in s1:
                diff.append(item)
        return diff

    ## Search a list for any item in the list of items to compare,
    ## and return True if one item is matched.
    @staticmethod
    def HasOneOf(search_list, compare_items):
        for search_item in search_list:
            if search_item in compare_items:
                return True
        return False

    ## Search a list for any item in the list of items to compare,
    ## and return True if one item is matched.
    @staticmethod
    def HasOneOfS(search_list, compare_items):
        return bool(set(search_list).intersection(set(compare_items)))

    ## Search a list for all item in the list of items to compare,
    ## and return True if all items in the compare list are matched.
    @staticmethod
    def HasAllOf(search_list, compare_items):
        for compare_item in compare_items:
            if compare_item not in search_list:
                return False
        return False if not search_list or not compare_items else True

    ## Search for one of the sub-strings in a search string.
    @staticmethod
    def HasOneOfSubString(search_string, sub_strings):
        for sub_str in sub_strings:
            if sub_str and search_string.find(sub_str) != -1:
                return True
        return False

    ## Search for all of the sub-strings in a search string.
    @staticmethod
    def HasAllOfSubString(search_string, sub_strings):
        for sub_str in sub_strings:
            if not sub_str or search_string.find(sub_str) == -1:
                return False
        return False if not search_string or not sub_strings else True

    ## Search through string for sub-strings in all list matching,
    ## and at least one of the sub-strings from any list.
    @staticmethod
    def HasAllAnySubString(search_string, all_sub_strings, any_sub_strings):
        for sub_str in all_sub_strings:
            if not sub_str or search_string.find(sub_str) == -1:
                return False
        for sub_str in any_sub_strings:
            if sub_str and search_string.find(sub_str) != -1:
                return True
        return False if not search_string or not all_sub_strings or not any_sub_strings else True
